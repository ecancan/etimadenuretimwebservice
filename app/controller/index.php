<?php
if(!empty($_SESSION["logged"]) && $_SESSION["logged"] == "success"){
    header("Location: ".URL."/processpage");
}

global $loginError;
if($_POST){
    $userData = post("login");
    if(!empty($userData["username"]) && !empty($userData["password"])){
        $loginResponse = loginProcess($userData["username"],$userData["password"]);
        if($loginResponse == "success"){
            header("Location: ".URL."/processpage");
        }else if($loginResponse == "The password is invalid or the user does not have a password."){
            $loginError = "Şifre hatalı.";
        }else if($loginResponse == "The email address is invalid."){
            $loginError = "Bu mail adreisne ait bir kullanıcı bulunamadı.";
        }else if($loginResponse == "There is no user record corresponding to this identifier. The user may have been deleted."){
            $loginError = "Bu bilgilere ait bir kullanıcı mevcut değil.";
        }else{
            $loginError = $loginResponse;
        }
    }else{
        $loginError = "Lütfen boş alan bırakmayınız.";
    }
}

require view('index');