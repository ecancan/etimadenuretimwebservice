<?php
/**
 * Created by PhpStorm.
 * User: Ecancan
 * Date: 4.03.2019
 * Time: 00:56
 */
if(!$_SESSION["logged"] == "success"){
    header("Location: ".URL);
}
global $uploadError;
global $uploadSuccess;

if($_POST){
    $facility = post("facility");
    $exelData = $_FILES["exeldata"];
    if(!empty($facility)){
        if($exelData["error"] != 4){
            if(is_uploaded_file($exelData["tmp_name"])){
                $accessFile = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                if($accessFile == $exelData["type"]){
                    $fileName = strtotime("now")."_".md5(strtotime("now"))."_".$exelData["name"];
                    $upload = move_uploaded_file($exelData["tmp_name"],PATH."/docs/".$fileName);
                    $uploadSuccess = addXmlData(PATH."/docs/".$fileName,$facility);
                    unset($_POST);
                }else{
                    $uploadError = "Sadece .Xlxs uzantılı dosya yüklenebilir.";
                }
            }else{
                $uploadError = "Dosya yüklenirken bir problem oluştu.";
            }
        }else{
            $uploadError = "Lütfen bir exel dosyası seçiniz.";
        }
    }else{
        $uploadError = "Lütfen bir tesis seçin.";
    }
}
require view("processpage");