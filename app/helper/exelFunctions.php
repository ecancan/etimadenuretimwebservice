<?php
/**
 * Created by PhpStorm.
 * User: Ecancan
 * Date: 2.03.2019
 * Time: 19:11
 */

/*
CREATE TABLE `uretim`.`uretimler` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `date` DATE NOT NULL DEFAULT '0000-00-00' , `facility` VARCHAR(255) NOT NULL , `shift` INT(11) NOT NULL , `orders` BIGINT NOT NULL , `material` BIGINT NOT NULL , `materialdesc` TEXT CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL , `weight` BIGINT NOT NULL , `weighttype` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL , `package_palette` BIGINT NOT NULL , `size` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL , `baggedShape` VARCHAR(255) CHARACTER SET utf16 COLLATE utf16_turkish_ci NOT NULL , `bagType` VARCHAR(255) CHARACTER SET utf16 COLLATE utf16_turkish_ci NOT NULL , `C` INT(11) NOT NULL , `A` INT(11) NOT NULL , `B` INT(11) NOT NULL , `C2` INT(11) NOT NULL , `K` INT(11) NOT NULL , `palette` VARCHAR(255) CHARACTER SET utf16 COLLATE utf16_turkish_ci NOT NULL , `packageType` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
*/


function addXmlData($path,$facility){
    global $db;
    //    $inputFileType = 'Xls';
    //    $inputFileType = 'Xml';
    //    $inputFileType = 'Ods';
    //    $inputFileType = 'Slk';
    //    $inputFileType = 'Gnumeric';
    //    $inputFileType = 'Csv';
    //    $inputFileType = 'Xlsx';
    $inputFileName = $path;
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = $reader->load($inputFileName);
    $worksheet = $spreadsheet->getActiveSheet();
    $dataAddCountSuccess = 0;
    $dataAddCountErrors = 0;
    foreach ($worksheet->getRowIterator() as $row) {
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(FALSE);
        $repoArray = array();
        foreach ($cellIterator as $cell) {
            if(!empty($cell->getValue())){
                if($cell->getColumn() == "A"){
                    $repoArray[$cell->getColumn()] = strtotime($cell->getFormattedValue());
                }else{
                    $repoArray[$cell->getColumn()] = $cell->getValue();
                }
            }else{
                $repoArray[$cell->getColumn()] = "";
            }
        }
        $query = $db->prepare('INSERT INTO uretimler SET 
          date = ?,
          facility = ?,
          shift = ?,
          orders = ?,
          material = ?,
          materialdesc = ?,
          weight = ?,
          weighttype = ?,
          package_palette = ?,
          size = ?,
          baggedShape = ?,
          bagType = ?,
          C = ?,
          A = ?,
          B = ?,
          C2 = ?,
          K = ?,
          palette = ?,
          packageType = ?,
          status = ?
        ');
        $add = $query->execute([
                date("Y-m-d", $repoArray["A"]),
                $facility,
                $repoArray["B"],
                $repoArray["E"],
                $repoArray["F"],
                $repoArray["G"],
                $repoArray["H"],
                $repoArray["I"],
                $repoArray["J"],
                $repoArray["L"],
                $repoArray["M"],
                $repoArray["N"],
                $repoArray["O"],
                $repoArray["P"],
                $repoArray["Q"],
                $repoArray["R"],
                $repoArray["S"],
                $repoArray["T"],
                $repoArray["U"],
                0
            ]
        );
        if($add){
            $dataAddCountSuccess++;
        }else{
            $dataAddCountErrors++;
        }
    }
    return "Eklenen Veri Sayısı : " . $dataAddCountSuccess . " -- Eklenemeyen Veri Sayısı : " . $dataAddCountErrors;
}