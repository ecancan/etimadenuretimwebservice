<?php
/**
 * Created by PhpStorm.
 * User: Ecancan
 * Date: 11.03.2019
 * Time: 12:21
 */

function dynamicCustomListQuery($paramArray){
    global $db;
    //$paramArray = array();
    //$paramArray["date"] = "";
    //$paramArray["facility"] = 3;
    //$paramArray["shift"] = 1;
    //$paramArray["order"] = "";
    //$paramArray["meterial"] = "";
    //$paramArray["meterialdesc"] = "";
    //$paramArray["weight"] = "";
    //$paramArray["weighttype"] = "";
    //$paramArray["package_palette"] = "";
    //$paramArray["size"] = "";
    //$paramArray["baggedShape"] = "";
    //$paramArray["bagType"] = "";
    //$paramArray["C"] = "";
    //$paramArray["A"] = "";
    //$paramArray["B"] = "";
    //$paramArray["C2"] = "";
    //$paramArray["K"] = "";
    //$paramArray["palette"] = "";
    //$paramArray["packageType"] = "";
    //$paramArray["statu"] = "";
    $executeArray = array();
    $queryString = "";
    $counter = 0;
    if(!empty(array_filter($paramArray))){
        foreach($paramArray as $key => $val){
            $counter++;
            $and = " AND ";
            if(!empty($val)){
                $executeArray[":".$key] = $val;
                $queryString .= $key." = :".$key.$and;
            }
        }
        $queryString = rtrim($queryString, " AND ");

        $statement = $db->prepare("SELECT * FROM uretimler where ".$queryString);
        $statement->execute($executeArray);
    }else{
        $statement = $db->prepare("SELECT * FROM uretimler");
        $statement->execute();
    }
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    $jsonArray = array();
    if($row){
        foreach ($row as $key => $value){
            $jsonArray["data"][] = $value;
        }
        $jsonArray["status"] = "success";
    }else{
        $jsonArray["status"] = "error";
    }
    $jsonOut = json_encode($jsonArray);
    return $jsonOut;
}