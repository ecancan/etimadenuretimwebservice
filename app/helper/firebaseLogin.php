<?php
/**
 * Created by PhpStorm.
 * User: Ecancan
 * Date: 3.03.2019
 * Time: 23:31
 */
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

function loginProcess($email,$password){
    $serviceAccount = ServiceAccount::fromJsonFile(PATH.'/etimadenuretim.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->create();

    try {
        $auth = $firebase->getAuth();
        $user = $auth->verifyPassword($email, $password);
        $_SESSION["userProperties"] = getRealtimeDB($user->uid);
        if($_SESSION["userProperties"]["userLevel"] == "Yönetici"){
            $_SESSION["userUid"] = $user->uid;
            $_SESSION["userMail"] = $user->email;
            $_SESSION["logged"] = "success";
            return "success";
        }else{
            return "Bu giriş sadece yetkililer içindir. Standart Kullanıcılar giriş yapamaz.";
        }

    } catch (Exception $e) {
        return $e->getMessage();
    }
}

function getRealtimeDB($childKey){
    $serviceAccount = ServiceAccount::fromJsonFile(PATH.'/etimadenuretim.json');
    $firebaseRealtime = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://etimadenuretim.firebaseio.com/')
        ->create();
    $database = $firebaseRealtime->getDatabase();
    $reference = $database->getReference('users/');
    return $reference->getSnapshot()->getChild($childKey)->getValue();
}