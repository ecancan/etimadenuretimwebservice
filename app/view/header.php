<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Etimaden Üretim Uygulaması Yetkili Girişi</title>
    <link rel="shortcut icon" href="" />
    <link rel="stylesheet" href="<?php echo public_url("css/bootstrap.min.css")?>" />
    <link rel="stylesheet" href="<?php echo public_url("style.css")?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css">
</head>
<body>
<header>
    <div class="headerContainer">
        <div class="container">
            <div class="headerContent">
                <div class="logo"><a href="<?php echo site_url();?>"><img src="<?php echo public_url("img/logo.png")?>" alt=""></a></div>
                <div class="menuContainer">
                    <ul>
                        <?php if(!empty($_SESSION["userMail"]) && !empty($_SESSION["userUid"])):?>
                            <li><a href="<?php echo site_url("logout");?>"><i class="fa fa-sign-out"></i> Çıkış</a></li>
                            <li><a href="javascript:void(0);">Üretim Girişi</a></li>
                        <?php else:?>
                            <li><a href="javascript:void(0);">Yetkili Girişi</a></li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</header>
<div class="mainContainer">
    <div class="container">
        <div class="mainContent">