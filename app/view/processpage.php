<?php require view("header");?>

    <div class="loadExelFileContainer">
        <div class="headerIcon">
            <i class="fa fa-upload" aria-hidden="true"></i>
        </div>
        <div class="loadExelFileContent">
            <form action="" method="POST"  enctype="multipart/form-data">
                <select class="selectFacility" name="facility">
                    <option value="1">Tesis 1</option>
                    <option value="2">Tesis 2</option>
                    <option value="3">Tesis 3</option>
                </select>
                <input type="file" class="fileUpload" name="exeldata">
                <button type="submit" class="uploadFormButton">Yükle</button>
            </form>
        </div>
    </div>
    <?php if(!empty($uploadError)):?>
        <div class="loginErrorContainer">
            <span class="errorContent"><?php echo $uploadError; ?></span>
        </div>
    <?php endif;?>
    <?php if(!empty($uploadSuccess)):?>
        <div class="loginErrorContainer">
            <span class="errorContent" style="background-color: #1e7e34"><?php echo $uploadSuccess; ?></span>
        </div>
    <?php endif;?>

<?php require view("footer");?>
