<?php require view('header'); ?>


    <div class="loginContainer">
        <div class="headerIcon">
            <i class="fa fa-user-o" aria-hidden="true"></i>
        </div>
        <div class="loginInputs">
            <form action="" method="post">
                <input type="text" name="login[username]" placeholder="Kullanıcı Adı">
                <input type="password" name="login[password]" placeholder="Şifre">
                <button type="submit">Giriş Yap</button>
            </form>
        </div>
    </div>
    <?php if(!empty($loginError)):?>
        <div class="loginErrorContainer">
            <span class="errorContent"><?php echo $loginError; ?></span>
        </div>
    <?php endif;?>
<?php require view('footer'); ?>

